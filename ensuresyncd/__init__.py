from __future__ import print_function
from multiprocessing import Process, Queue


import pyinotify


class EventHandler(pyinotify.ProcessEvent):
    def process_IN_CREATE(self, event):
        print("Creating: {}".format(event.pathname))

    def process_IN_DELETE(self, event):
        print("Removing:".format(event.pathname))


class SyncdController(object):
    
    def __init__(self):        
        wm = pyinotify.WatchManager()  # Watch Manager
        mask = pyinotify.IN_DELETE | pyinotify.IN_CREATE  # watched events        
        self.notifier = pyinotify.ThreadedNotifier(wm, EventHandler())
        self.notifier.start()        
        
        self.wdd = wm.add_watch('/tmp', mask, rec=True, auto_add=True)        
        notifier.stop()


class SyncdSlave(object):
    
    def __init__(self):
        pass
    


if __name__ == "__main__":   
    s = Syncd()